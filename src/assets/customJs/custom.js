(function() {
  var proxied = window.alert;
  window.alert = function() {
    modal = $('<div class="modal fade" id="successErrorModal">\n' +
      '  <div class="modal-dialog modal-dialog-centered">\n' +
      '    <div class="modal-content">\n' +
      '\n' +
      '      <!-- Modal body -->\n' +
      '      <div class="modal-body text-center">\n' +
      '        <div class="col-12 form-group">\n' +
      '          <h6></h6>\n' +
      '        </div>\n' +
      '      </div>\n' +
      '\n' +
      '     <!-- Modal footer -->\n' +
      '     <div class="col-12 text-center m-2">\n' +
      '      <button type="button" class="btn btn-danger btn btn-danger btn-sm" data-dismiss="modal">Close</button>\n' +
      '      </div>\n'+
      '    </div>\n' +
      '  </div>\n' +
      '</div>');
    modal.find(".modal-body").text(arguments[0]);
    modal.modal('show');
  };
})();


/*
(function(proxied) {

  window.alert = function() {
    // do something here
    return proxied.apply(this, arguments);
  };
})(window.alert);*/
