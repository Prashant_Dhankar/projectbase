import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';


import { AppRoutingModule, routingComponentsMain } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService }  from './services/auth.service';
import { AuthGuard } from './guard/auth.guard';
import { MyInterceptor } from './interceptor/my-interceptor';
import { ForbiddenComponent } from './modules/shared/components/forbidden/forbidden.component';


@NgModule({
  declarations: [
    AppComponent, ForbiddenComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routingComponentsMain,
    AppRoutingModule
  ],
  providers: [AuthService, AuthGuard, 
  {provide: HTTP_INTERCEPTORS, useClass: MyInterceptor, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
