
import { Injectable} from '@angular/core';
import {HttpInterceptor,
        HttpResponse,
        HttpRequest,
        HttpErrorResponse,
        HttpEvent,
        HttpHandler} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class MyInterceptor implements HttpInterceptor {

    userToken = '123';
    intercept(
         req: HttpRequest<any>,
         next: HttpHandler):
         Observable<HttpEvent<any>> {
        let updatedRequest: any;

        updatedRequest = req.clone({
            setHeaders: {
                platform: 'web',
                authorization: `access_token=${this.userToken}`
            },
            url: req.url
        })

        return next.handle(updatedRequest).pipe(
            map((event: HttpEvent<any>) => {
             if(event instanceof HttpResponse) {
                 // do yor stuff
             }
                return event;   
            }),
            catchError((error: HttpErrorResponse) => {
              
                //  handle your error here;65432
                return throwError(error);
            })
          );
    }
}