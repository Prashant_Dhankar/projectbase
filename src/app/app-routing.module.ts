import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostAuthModule } from './modules/post-auth/post-auth.module';
import { PreAuthModule } from './modules/pre-auth/pre-auth.module';
import { ForbiddenComponent } from './modules/shared/components/forbidden/forbidden.component';


const routes: Routes = [
  {path:'', redirectTo: '/login', pathMatch: 'full'},
  {
    path: 'login',
    loadChildren: () => PreAuthModule
  }, {
    path: 'authentic',
    loadChildren: () => PostAuthModule
  }, {
    path: '**',
    component: ForbiddenComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponentsMain = [PreAuthModule, PostAuthModule];

