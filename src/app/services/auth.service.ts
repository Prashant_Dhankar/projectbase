import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  checkUserLogin() {
    // do you stuff and send status
    return true;
  }

  // function to logged out user from application
  logout() {
    this.clearLocalStorageData();
  // do your stuff
  }


// function to clear local storage data
  clearLocalStorageData() {
    localStorage.clear();
  }

  // function to encrypt data
  encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), 'secret key 123').toString();
    } catch (e) {
      console.log(e);
    }
  }
// function to decrypt data
  decryptData(data) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, 'secret key 123');
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }

}
