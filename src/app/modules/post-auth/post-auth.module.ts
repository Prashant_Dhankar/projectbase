import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostAuthRoutingModule, componentsForPostAuth } from './post-auth-routing.module';


@NgModule({
  declarations: [componentsForPostAuth],
  imports: [
    CommonModule,
    PostAuthRoutingModule
  ]
})
export class PostAuthModule { }
