import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthenticComponent } from './components/authentic/authentic.component';
import { ProfileComponent } from './components/profile/profile.component';
import { HeaderComponent } from './components/header/header.component';


const routes: Routes = [ 
  {path:'authentic', component: AuthenticComponent,
   children:[
     {path:'', redirectTo:'/dashboard', pathMatch: 'full' }, 
     {path:'dashboard', component: DashboardComponent},
     {path:'profile', component: ProfileComponent}
   ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostAuthRoutingModule { }

export const componentsForPostAuth = [
  DashboardComponent, ProfileComponent, AuthenticComponent, HeaderComponent
]
